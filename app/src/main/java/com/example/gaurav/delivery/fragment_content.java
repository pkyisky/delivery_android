package com.example.gaurav.delivery;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.example.gaurav.delivery.Backend.Backendserver;
import com.example.gaurav.delivery.Utils.TimeUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class fragment_content extends Fragment {

    private static Reports mActivity;
    private String time,deliveredcount,cod,totals,ongoingcount,cards;

//    private String tvContentValue,ordervalue,ongoingvalue;

    private TextView tvContent,order,ongoings,codtext,cardtext,totaltext;
    AsyncHttpClient client;

    private static final String KEY_DATE = "date";

    public static fragment_content newInstance(long date) {
        fragment_content fragmentFirst = new fragment_content();
        Bundle args = new Bundle();
        args.putLong(KEY_DATE, date);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (Reports) getActivity();
        Backendserver backend = new Backendserver(mActivity);
        //   getUserDetails();
        client = backend.getHTTPClient();

        final long millis = getArguments().getLong(KEY_DATE);
        if (millis > 0) {
            final Context context = getActivity();
            if (context != null) {
            //    tvContentValue = "This is the content for the date " +
               time=TimeUtils.getFormattedDate(context,millis);
              //  getreport();
            /*    order.setText(deliveredcount);
                ongoings.setText(ongoingcount);
                totaltext.setText(totals);
                cardtext.setText(cards);
                codtext.setText(cod); */
               /* Log.e("error",""+deliveredcount);
                Log.e("error",""+cod);
                Log.e("error",""+totals);
                Log.e("error",""+ongoingcount);
                Log.e("error",""+cards); */


                return;
            }
        }


        deliveredcount = "";
        time = "";
      //  ongoingvalue = "";
//        startCountAnimation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_content, container, false);
       // getreport();
        tvContent = (TextView) view.findViewById(R.id.tvContent);
        order = (TextView) view.findViewById(R.id.ordertxt);
        ongoings = (TextView) view.findViewById(R.id.ongoingtxt);
        cardtext = (TextView) view.findViewById(R.id.cardtxt);
        codtext = (TextView) view.findViewById(R.id.codtxt);
        totaltext = (TextView) view.findViewById(R.id.totaltxt);

        client.get(Backendserver.url + "/api/ecommerce/getDeliveredOrderDetails/?userId="+MainActivity.userPK+"&dated="+time, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    deliveredcount= response.getString("deliveredCount");
                    cod= response.getString("cod");
                    totals= response.getString("total");
                    ongoingcount= response.getString("ongoingCount");
                    cards= response.getString("card");
                    Log.e("error",""+deliveredcount);
                    Log.e("error",""+cod);
                    Log.e("error",""+totals);
                    Log.e("error",""+ongoingcount);
                    Log.e("error",""+cards);


                    order.setText(deliveredcount);
                    ongoings.setText(ongoingcount);
                    totaltext.setText(totals);
                    cardtext.setText(cards);
                    codtext.setText(cod);

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(mActivity, "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
            }
        });


     /*   order.setText(deliveredcount);
        ongoings.setText(ongoingcount);
        totaltext.setText(totals);
        cardtext.setText(cards);
        codtext.setText(cod); */
      /*  Log.e("error",""+deliveredcount);
        Log.e("error",""+cod);
        Log.e("error",""+totals);
        Log.e("error",""+ongoingcount);
        Log.e("error",""+cards);*/

           tvContent.setText(deliveredcount);
     //   ongoing.setText(ongoingvalue);
        return view;
    }
    private void startCountAnimation() {
        ValueAnimator animator = ValueAnimator.ofInt(0, 50);
        animator.setDuration(5000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                order.setText(animation.getAnimatedValue().toString());
            }
        });
        animator.start();
    }



    public void getreport(){

        client.get(Backendserver.url + "/api/ecommerce/getDeliveredOrderDetails/?userId="+MainActivity.userPK+"&dated="+time, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                   deliveredcount= response.getString("deliveredCount");
                   cod= response.getString("cod");
                   totals= response.getString("total");
                   ongoingcount= response.getString("ongoingCount");
                   cards= response.getString("card");
                    Log.e("error",""+deliveredcount);
                    Log.e("error",""+cod);
                    Log.e("error",""+totals);
                    Log.e("error",""+ongoingcount);
                    Log.e("error",""+cards);


                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(mActivity, "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
            }
        });

    }


}