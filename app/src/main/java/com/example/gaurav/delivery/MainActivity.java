package com.example.gaurav.delivery;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gaurav.delivery.Adapter.Adapter_offine;
import com.example.gaurav.delivery.Adapter.mainadapter;
import com.example.gaurav.delivery.Backend.Backendserver;
import com.example.gaurav.delivery.Backend.SessionManager;
import com.example.gaurav.delivery.entities.Item;
import com.example.gaurav.delivery.entities.product_item;
import com.google.zxing.Result;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import dmax.dialog.SpotsDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class MainActivity extends AppCompatActivity  implements ZXingScannerView.ResultHandler{

    ImageView btn_logout,reports;
    Button delivered;
    TextView username1,scanbtn,empty,cancel,partno_dialogtxt,grandtotaltxt;
    private ZXingScannerView zXingScannerView;
    SessionManager sessionManager;
    RecyclerView listitem;
    Dialog dialog;
    AlertDialog.Builder builder;
     mainadapter adapter;
     Adapter_offine adapter_offine;
   //  ArrayList<Item>arrayList;
     ArrayList<product_item>prodlist;
    ArrayList<Item>prodlistdelivered;
    private AsyncHttpClient client;
    public static String username = "";
    public static String userPK = "";
    Context context;
    String resultcode,already="",status="";
    String pk;
    String grandTotal;

    int count=0,countpatch=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username1=findViewById(R.id.usernametxt);
        btn_logout=findViewById(R.id.btn_logout);
        grandtotaltxt=findViewById(R.id.Grandtotaltxt);

        listitem=findViewById(R.id.listitem);
        empty=findViewById(R.id.empty);
        reports=findViewById(R.id.reportsimg);
        //arrayList=new ArrayList<>();
        prodlist=new ArrayList<>();
        prodlistdelivered=new ArrayList<>();


        sessionManager = new SessionManager(getApplicationContext());
        Backendserver backend = new Backendserver(getApplicationContext());
        //   getUserDetails();
        client = backend.getHTTPClient();
        checklogin();
        getUserDetails();

        askForPermission(Manifest.permission.CAMERA, CAMERA);
        zXingScannerView = new ZXingScannerView(this);
        zXingScannerView = (ZXingScannerView) findViewById(R.id.zscan);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();

  if(prodlist.size()==0)
  {
      empty.setVisibility(View.VISIBLE);
  }

        builder = new AlertDialog.Builder(this);
        btn_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //Uncomment the below code to Set the message and title from the strings.xml file
                //builder.setMessage("jsdjvsd") .setTitle("sjbasjcb");
                //Setting message manually and performing action on button click
                builder.setMessage("Do you want to Log out ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sessionManager.logoutUser();
                                finish();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Log out");
                alert.show();
            }



        });
       // statuspatch();
       // getproduct();
        reports();
    }

    @Override
    protected void onResume() {
        super.onResume();
       //do something
        askForPermission(Manifest.permission.CAMERA, CAMERA);
        zXingScannerView = new ZXingScannerView(this);
        zXingScannerView = (ZXingScannerView) findViewById(R.id.zscan);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

    private Boolean exit = false;

    @Override
    public void onBackPressed() {

        if (exit) {
            super.onBackPressed();
        } else {
            Toast.makeText(this, "Press again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    public void checklogin() {
        // Check login status
        if (sessionManager.getUsername().equals("")) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(this, Login.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            this.startActivity(i);
            finish();
        }

    }

    public void getUserDetails() {
        //  btn_logout.setVisibility(View.GONE);
        //  deliveryAction.setVisibility(View.GONE);
        //  btn_logout.setVisibility(View.GONE);

        client.get(Backendserver.url + "/api/HR/users/?mode=mySelf&format=json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                Log.e("MainActivity", "onSuccess");
                super.onSuccess(statusCode, headers, response);
                // btn_logout.setVisibility(View.VISIBLE);
                // deliveryAction.setVisibility(View.VISIBLE);
                // username1.setVisibility(View.VISIBLE);
                try {

                    JSONObject usrObj = response.getJSONObject(0);
                    userPK = usrObj.getString("pk");
                    username = usrObj.getString("username");
                    String firstName = usrObj.getString("first_name");
                    String lastName = usrObj.getString("last_name");
                    //    String email = usrObj.getString("email");
                    JSONObject profileObj = usrObj.getJSONObject("profile");

                    String dpLink = profileObj.getString("displayPicture");
                    if (dpLink.equals("null") || dpLink == null) {
                        dpLink = Backendserver.url + "static/images/userIcon.png";
                    }
                    String mobile = profileObj.getString("mobile");
                    username1.setText(firstName + " " + lastName);
                    Uri uri = Uri.parse(dpLink);
                    //userImage.setImageURI(uri);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //getCartCount();
                //Toast.makeText(getApplicationContext(),"cartSize",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e("MainActivity", "onFailure");
            }

            @Override
            public void onFinish() {
                super.onFinish();
                Log.e("MainActivity", "onFinish");
            }
        });
    }


    @Override
    public void handleResult(Result result) {

        resultcode=result.toString();
        int length=resultcode.length();

       if(length<5)
        {
            Log.e("already in the list" ,"1");
            for (int i = 0; i < prodlist.size(); i++) {
                already = prodlist.get(i).getCourierAWBNo();

                if (prodlist.get(i).getStatus().equals("outForDelivery") && prodlist.get(i).getCourierAWBNo().equals(resultcode)) {
                    // Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();

                    if (count == 0) {
                        Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(200);
                        Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();
                        MyCustomAlertDialog2();
                    }
                    count++;

                }
                if (prodlist.get(i).getStatus().equals("delivered") && prodlist.get(i).getCourierAWBNo().equals(resultcode)) {
                    Toast.makeText(this, "Already delivered", Toast.LENGTH_SHORT).show();
                    //MyCustomAlertDialog2();

                }

                if (already.equals(resultcode)) {
                    Log.e("already in the list", already);
                    break;
                }

            }
            if (already.equals(resultcode)) {
                Log.e("already in the list", already);
                Toast.makeText(getApplicationContext(), "Already in the list ", Toast.LENGTH_SHORT).show();
            } else {

                //    if(status.equalsIgnoreCase("delivered"))
                //  {
                Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(200);
                Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();
                //      Toast.makeText(getApplicationContext(),"Already delivered",Toast.LENGTH_SHORT).show();
                //  }
                //  else {
                getproduct();
                //  }
            }

        }


    else {
           Log.e("already in the list" ,"2");

            String a = resultcode.substring(0, 5);

            if (a.equalsIgnoreCase("BCOFF")) {
                Log.e("already in the list" ,"3");
                Toast.makeText(getApplicationContext(), "" + result.toString() + a, Toast.LENGTH_SHORT).show();

                for (int i = 0; i < prodlistdelivered.size(); i++) {
                    already = prodlistdelivered.get(i).getBarcode();

                    if (prodlistdelivered.get(i).getStatus1().equals("outForDelivery") && prodlistdelivered.get(i).getBarcode().equals(resultcode)) {
                        // Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();

                        if (count == 0) {
                            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(200);
                            Toast.makeText(this, "barcode : "+result.toString(), Toast.LENGTH_SHORT).show();
                            dialogoffline();
                        }
                        count++;

                    }
                    if (prodlistdelivered.get(i).getStatus1().equals("Completed") && prodlistdelivered.get(i).getBarcode().equals(resultcode)) {
                        Toast.makeText(this, "Already delivered", Toast.LENGTH_SHORT).show();
                        //MyCustomAlertDialog2();

                    }

                    if (already.equals(resultcode)) {
                        Log.e("already in the list", already);
                        break;
                    }

                }
                if (already.equals(resultcode)) {
                    Log.e("already in the list", already);
                   // Toast.makeText(getApplicationContext(), "Already in the list ", Toast.LENGTH_SHORT).show();
                } else {

                    //    if(status.equalsIgnoreCase("delivered"))
                    //  {
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();
                    //      Toast.makeText(getApplicationContext(),"Already delivered",Toast.LENGTH_SHORT).show();
                    //  }
                    //  else {
                    getproductoffline();
                    //  }
                }

            } else {

                for (int i = 0; i < prodlist.size(); i++) {
                    already = prodlist.get(i).getCourierAWBNo();

                    if (prodlist.get(i).getStatus().equals("outForDelivery") && prodlist.get(i).getCourierAWBNo().equals(resultcode)) {
                        // Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();

                        if (count == 0) {
                            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(200);
                            Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();
                            MyCustomAlertDialog2();
                        }
                        count++;

                    }
                    if (prodlist.get(i).getStatus().equals("delivered") && prodlist.get(i).getCourierAWBNo().equals(resultcode)) {
                        Toast.makeText(this, "Already delivered", Toast.LENGTH_SHORT).show();
                        //MyCustomAlertDialog2();

                    }

                    if (already.equals(resultcode)) {
                        Log.e("already in the list", already);
                        break;
                    }

                }
                if (already.equals(resultcode)) {
                    Log.e("already in the list", already);
                   // Toast.makeText(getApplicationContext(), "Already in the list ", Toast.LENGTH_SHORT).show();
                } else {

                    //    if(status.equalsIgnoreCase("delivered"))
                    //  {
                    Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(200);
                    Toast.makeText(this, "barcode : " + result.toString(), Toast.LENGTH_SHORT).show();
                    //      Toast.makeText(getApplicationContext(),"Already delivered",Toast.LENGTH_SHORT).show();
                    //  }
                    //  else {
                    getproduct();
                    //  }
                }

            }

            //    getinventorydelivered();

       }
        zXingScannerView.resumeCameraPreview(this);

    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            //asking for permission prompt
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                //request for permissions and getting request code -- 5 for camera
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);

            } else {
                //requesting permission
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else {
            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }

    //Response for RunTime Permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                //Camera
                case 5:
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 12);
                    }
                    // askForPermission(Manifest.permission.CAMERA,CAMERA);
                    break;

            }

            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
            zXingScannerView = new ZXingScannerView(this);
            zXingScannerView = (ZXingScannerView) findViewById(R.id.zscan);
            zXingScannerView.setResultHandler(this);
            zXingScannerView.startCamera();


        } else {
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }


    public void getproduct(){


        final android.app.AlertDialog alertDialog= new SpotsDialog.Builder().setContext(this).build();
        alertDialog.show();

        client.get( Backendserver.url+ "/api/ecommerce/orderQtyMap/?courierAWBNo="+resultcode,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                alertDialog.dismiss();
                empty.setVisibility(View.GONE);
                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);
                        pk = object.getString("pk");
                        status = object.getString("status");

                          Log.e("errror"," "+pk);
                        if(status.equalsIgnoreCase("delivered"))
                          {
                              Log.e("errror"," "+status);
                              Toast.makeText(getApplicationContext(), "Already delivered ", Toast.LENGTH_SHORT).show();
                        }
                        if(status.equalsIgnoreCase("outForDelivery"))
                        {   countpatch++;
                            getupdatedproduct();
                        }

                    if(status.equalsIgnoreCase("packed")||status.equalsIgnoreCase("created")){
                              RequestParams param = new RequestParams();
                              param.put("status", "outForDelivery");
                              param.put("userId", Integer.parseInt(userPK));

                              client.patch(Backendserver.url + "/api/ecommerce/orderQtyMap/" + pk + "/", param, new AsyncHttpResponseHandler() {
                                  @Override
                                  public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                      Log.e("errror", " fvfv" );

                                     countpatch++;
                                      //  Toast.makeText(getApplicationContext(), "Status updated ", Toast.LENGTH_SHORT).show();
                                      getupdatedproduct();

                                      //mContext.startActivity(new Intent(mContext, CartListActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));//comment
                                  }

                                  @Override
                                  public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                      Log.e("errror", " " + error + responseBody + statusCode);
                                  }
                              });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


                //listshowrcy.setAdapter(new search_parts_adapter(productlists));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(),"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });



    }


    public void getproductoffline(){


        final android.app.AlertDialog alertDialog= new SpotsDialog.Builder().setContext(this).build();
        alertDialog.show();

        client.get( Backendserver.url+"/api/POS/invoice/?barCVal=" + resultcode,new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                alertDialog.dismiss();
                empty.setVisibility(View.GONE);
                //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                for (int i = 0; i < response.length(); i++) {

                    JSONObject object=null;

                    try {
                        object = response.getJSONObject(i);
                        pk = object.getString("pk");
                        status = object.getString("status");

                        Log.e("errror"," "+pk);
                        if(status.equalsIgnoreCase("Completed"))
                        {
                            Log.e("errror"," "+status);
                            Toast.makeText(getApplicationContext(), "Already delivered ", Toast.LENGTH_SHORT).show();
                        }
                        if(status.equalsIgnoreCase("outForDelivery"))
                        {
                            countpatch++;
                            getoffine();
                        }
                      if(status.equalsIgnoreCase("In Progress")||status.equalsIgnoreCase("Created")){
                            RequestParams param = new RequestParams();
                            param.put("status", "outForDelivery");
                            param.put("userId",Integer.parseInt(userPK));

                            Log.e("errror", " " + param);
                            client.patch(Backendserver.url + "/api/POS/invoice/" + pk + "/", param, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    Toast.makeText(getApplicationContext(), "Status updated ", Toast.LENGTH_SHORT).show();
                                    countpatch++;
                                    //  Toast.makeText(getApplicationContext(), "Status updated ", Toast.LENGTH_SHORT).show();
                                    getoffine();

                                    //mContext.startActivity(new Intent(mContext, CartListActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));//comment
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                    Log.e("errror", " " + error + responseBody + statusCode);
                                }
                            });
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


                //listshowrcy.setAdapter(new search_parts_adapter(productlists));

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(),"data not fetching"+errorResponse,Toast.LENGTH_SHORT);
            }
        });



    }





    public void getoffine(){
      //  prodlist.clear();


          //  Toast.makeText(getApplicationContext(), "Status updated ", Toast.LENGTH_SHORT).show();

            client.get(Backendserver.url + "/api/POS/invoice/?barCVal=" + resultcode, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    empty.setVisibility(View.GONE);
                    //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object1 = null;

                        try {
                            object1 = response.getJSONObject(i);

                            String product=object1.getString("products");
                            JSONArray jArray = new JSONArray(product);

                              for(int j=0;j< jArray.length();j++)
                              {
                                  JSONObject obj=jArray.getJSONObject(j);
                                  JSONObject obj1=obj.getJSONObject("data");

                                  JSONObject pro= obj1.getJSONObject("product");

                                  String name=pro.getString("name");
                                  String price=pro.getString("price");

                                  String quantity=obj.getString("quantity");

                                  String barcode=object1.getString("barCVal");
                                  status=object1.getString("status");
                                  grandTotal=object1.getString("grandTotal");
                                  Log.e("errror", " " + price);
                                  Log.e("errror", " " + quantity);
                                  Log.e("errror", " " + barcode);
                                  Log.e("errror", " " + name);
                                  Log.e("errror", " " + status);
                                  Log.e("errror", " " + grandTotal);
                                  object1.put("name",name);
                                  object1.put("price",price);
                                  object1.put("quantity",quantity);
                                  object1.put("barCVal",barcode);
                                  object1.put("status",status);
                                  object1.put("grandTotal",grandTotal);

                                  prodlistdelivered.add(new Item(object1));

                              }
                              grandtotaltxt.setText(grandTotal);


                            Log.e("errror", " " + object1);

                         //   Log.e("errror", " " + jArray);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                    listitem.setHasFixedSize(true);
                    listitem.setLayoutManager(linearLayoutManager);
                    adapter_offine = new Adapter_offine(prodlistdelivered, MainActivity.this);
                    //listshowgrn_detail.setLayoutManager(new GridLayoutManager(this,3));
                    listitem.setAdapter(adapter_offine);
                    adapter_offine.notifyDataSetChanged();
                    //listshowrcy.setAdapter(new search_parts_adapter(productlists));

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(getApplicationContext(), "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
                }
            });
    }
    public void getupdatedproduct(){
          if(countpatch==1)
        {

             prodlist.clear();
              client.get(Backendserver.url + "/api/ecommerce/orderQtyMap/?courierAWBNo=" + resultcode, new JsonHttpResponseHandler() {
                  @Override
                  public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                      super.onSuccess(statusCode, headers, response);

                      empty.setVisibility(View.GONE);
                      int totalSum=0 ;
                      //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                      for (int i = 0; i < response.length(); i++) {

                          JSONObject object1 = null;

                          try {
                              object1 = response.getJSONObject(i);

                             int ppdiscount=object1.getInt("ppAfterDiscount");
                             int qty=object1.getInt("qty");
                             int pay=ppdiscount*qty;
                              totalSum=totalSum+pay;
                              String totalsum=Integer.toString(totalSum) ;
                              grandtotaltxt.setText(totalsum);

                              String payable=Integer.toString(pay) ;

                              object1.put("ppdiscount",payable);

                              Log.e("errror", " " + pk);
                              prodlist.add(new product_item(object1));
                            ;
                              //  Log.e("error"," "+prodlist.toString());

                          } catch (JSONException e) {
                              e.printStackTrace();
                          }

                      }
                      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                      listitem.setHasFixedSize(true);
                      listitem.setLayoutManager(linearLayoutManager);
                      adapter = new mainadapter(prodlist, MainActivity.this);
                      // listshowgrn_detail.setLayoutManager(new GridLayoutManager(this,3));
                      listitem.setAdapter(adapter);
                      adapter.notifyDataSetChanged();
                      //listshowrcy.setAdapter(new search_parts_adapter(productlists));

                  }

                  @Override
                  public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                      super.onFailure(statusCode, headers, throwable, errorResponse);
                      Toast.makeText(getApplicationContext(), "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
                  }
              });


          }


    }
    public void getupdateddelivery(){
        if(countpatch==1) {
           // Toast.makeText(getApplicationContext(), "Status updated ", Toast.LENGTH_SHORT).show();

            client.get(Backendserver.url + "/api/ecommerce/orderQtyMap/?courierAWBNo=" + resultcode, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    empty.setVisibility(View.GONE);
                    //        Toast.makeText(getApplicationContext(),"res "+response.toString(),Toast.LENGTH_SHORT).show();
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject object1 = null;

                        try {
                            object1 = response.getJSONObject(i);

                            Log.e("errror", " " + pk);
                            prodlist.add(new product_item(object1));
                            ;
                            //  Log.e("error"," "+prodlist.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Toast.makeText(getApplicationContext(), "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
                }
            });

        }
    }


        public void MyCustomAlertDialog2() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogbox);
        dialog.setTitle("My Custom Dialog");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        delivered = (Button) dialog.findViewById(R.id.delivered);
        cancel = dialog.findViewById(R.id.txt_cancel);
        partno_dialogtxt = dialog.findViewById(R.id.partno_dialog_project);

        partno_dialogtxt.setText(resultcode);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
                RequestParams params=new RequestParams();
                params.put("userId",Integer.parseInt(userPK));
                params.put("barcode",resultcode);
             //   params.put("typ","card");
                params.put("typ","online");
                params.put("modeOfPayment","card");


                client.post(Backendserver.url + "/api/ecommerce/postBarcode/",params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(getApplicationContext(),"Delivered successfully",Toast.LENGTH_SHORT).show();
                        //   Grn_item_detail grn_item_detail1 = new Grn_item_detail("Part no : "+partno, qty,productpk,"Barcode no : "+barcode,description1,Price);
                      //  prodlist.clear();
                       // getupdateddelivery();
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();
                        //  Grnlists_detail.add(grn_item_detail1);
                        //  notifyDataSetChanged();
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(context,"unsuccess ",Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
        delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                RequestParams params=new RequestParams();
                params.put("userId",Integer.parseInt(userPK));
                params.put("barcode",resultcode);
                params.put("typ","online");
                params.put("modeOfPayment","cash");

                client.post(Backendserver.url + "/api/ecommerce/postBarcode/",params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(getApplicationContext(),"Delivered successfully",Toast.LENGTH_SHORT).show();
                        //   Grn_item_detail grn_item_detail1 = new Grn_item_detail("Part no : "+partno, qty,productpk,"Barcode no : "+barcode,description1,Price);
                            // prodlist.clear();
                           //  getinventorydelivered();
                         // getupdateddelivery();
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();
                         //  Grnlists_detail.add(grn_item_detail1);
                          // notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(context,"unsuccess ",Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });

        // Window window = dialog.getWindow();

        // window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.show();

        // Window window = dialog.getWindow();

    }
    public void dialogoffline() {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogbox);
        dialog.setTitle("My Custom Dialog");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        delivered = (Button) dialog.findViewById(R.id.delivered);
        cancel = dialog.findViewById(R.id.txt_cancel);
        partno_dialogtxt = dialog.findViewById(R.id.partno_dialog_project);

        partno_dialogtxt.setText(resultcode);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();
                RequestParams params=new RequestParams();
                params.put("userId",Integer.parseInt(userPK));
                params.put("barcode",resultcode);
                params.put("typ","offline");
                params.put("modeOfPayment","card");
                client.post(Backendserver.url + "/api/ecommerce/postBarcode/",params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(getApplicationContext(),"Delivered successfully",Toast.LENGTH_SHORT).show();
                        //   Grn_item_detail grn_item_detail1 = new Grn_item_detail("Part no : "+partno, qty,productpk,"Barcode no : "+barcode,description1,Price);
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();
                        //  Grnlists_detail.add(grn_item_detail1);
                        //  notifyDataSetChanged();
                    }
                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(context,"unsuccess ",Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
        delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                RequestParams params=new RequestParams();
                params.put("userId",Integer.parseInt(userPK));
                params.put("barcode",resultcode);
                params.put("typ","offline");
                params.put("modeOfPayment","cash");

                client.post(Backendserver.url + "/api/ecommerce/postBarcode/",params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(getApplicationContext(),"Delivered successfully",Toast.LENGTH_SHORT).show();
                        //   Grn_item_detail grn_item_detail1 = new Grn_item_detail("Part no : "+partno, qty,productpk,"Barcode no : "+barcode,description1,Price);
                        // prodlist.clear();
                        //  getinventorydelivered();
                        // getupdateddelivery();
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();
                        //  Grnlists_detail.add(grn_item_detail1);
                        // notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(context,"unsuccess ",Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });

        // Window window = dialog.getWindow();

        // window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        dialog.show();

        // Window window = dialog.getWindow();

    }
    public void reports()
    {
        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(getApplicationContext(),Reports.class);
                startActivity(i);
            }
        });
    }


}
