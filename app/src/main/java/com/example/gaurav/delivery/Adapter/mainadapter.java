package com.example.gaurav.delivery.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gaurav.delivery.Backend.Backendserver;
import com.example.gaurav.delivery.MainActivity;
import com.example.gaurav.delivery.R;
import com.example.gaurav.delivery.entities.Item;
import com.example.gaurav.delivery.entities.product_item;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class mainadapter extends RecyclerView.Adapter<mainadapter.Holderview>  {
    private AsyncHttpClient client;
    Dialog dialog;
    Button delivered;
    TextView username1,scanbtn,empty,cancel,partno_dialogtxt;
    String barcodeno,productname,status,total;


    private ArrayList<product_item> list;
    private Context context;
    public mainadapter(ArrayList<product_item> list, Context context) {
        this.list = list;
        this.context = context;
    }
    @Override
    public mainadapter.Holderview onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.cardview,parent,false);
        return new mainadapter.Holderview(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull Holderview holder, final int position) {

        product_item item=list.get(position);
       // String totalamount=Integer.toString(Integer.parseInt(item.getPpAfterDiscount())*Integer.parseInt(item.getQty()));

        holder.v_name.setText(" "+item.getProductName());
        holder.qty.setText("Quantity : "+item.getQty());
        holder.totalAmount.setText("Payable amount : "+item.getAmountpayable());
        holder.productPrice.setText("Product price : "+item.getProductPrice());
        holder.discount.setText("Discount amount : "+item.getDiscountAmount());
        holder.status.setText("Status : "+item.getStatus());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              barcodeno=list.get(position).getCourierAWBNo();
              productname=list.get(position).getProductName();
              status=list.get(position).getStatus();
              if(status.equals("delivered"))
              {
                  Toast.makeText(context,"Item already delivered",Toast.LENGTH_SHORT).show();
              }
              else {
               //   MyCustomAlertDialog2();
              }
                //Toast.makeText(context, "click on " + list.get(position).getBarcode(),
                      //  Toast.LENGTH_SHORT).show();
           //     context.startActivity(new Intent(context, Grn_project_details.class).putExtra("projectpk", Grnlist.get(position).getPk()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class Holderview extends RecyclerView.ViewHolder
    {

        TextView v_name,qty,productPrice,totalAmount,status,discount;
        Holderview(View itemview)
        {
            super(itemview);

            v_name = (TextView) itemView.findViewById(R.id.companyname_grn);
            qty=itemview.findViewById(R.id.servicename);
            productPrice=itemview.findViewById(R.id.mobile);
            status=itemview.findViewById(R.id.status);
            totalAmount=itemview.findViewById(R.id.date);
            discount=itemview.findViewById(R.id.discounttxt);
        }
    }

    public void MyCustomAlertDialog2() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogbox);
        dialog.setTitle("My Custom Dialog");


        delivered = (Button) dialog.findViewById(R.id.delivered);
        cancel = dialog.findViewById(R.id.txt_cancel);
        partno_dialogtxt = dialog.findViewById(R.id.partno_dialog_project);

        partno_dialogtxt.setText(productname);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();

            }
        });
        delivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Backendserver backend = new Backendserver(context);
                //   getUserDetails();
                client = backend.getHTTPClient();
                dialog.cancel();
               RequestParams params=new RequestParams();
                params.put("userId",Integer.parseInt(MainActivity.userPK));
                params.put("barcode",barcodeno);

                client.post(Backendserver.url + "/api/ecommerce/postBarcode/",params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Toast.makeText(context,"Delivered successfully",Toast.LENGTH_SHORT).show();
                        // Grn_item_detail grn_item_detail1 = new Grn_item_detail("Part no : "+partno, qty,productpk,"Barcode no : "+barcode,description1,Price);
                        // Grnlists_detail.add(grn_item_detail1);
                         notifyDataSetChanged();

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Toast.makeText(context,"unsuccess ",Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });

        //   Window window = dialog.getWindow();
        //    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.show();

        //   Window window = dialog.getWindow();



    }

}

