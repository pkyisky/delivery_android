package com.example.gaurav.delivery;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import com.example.gaurav.delivery.Adapter.mainadapter;
import com.example.gaurav.delivery.Adapter.reportadapter;
import com.example.gaurav.delivery.Backend.Backendserver;
import com.example.gaurav.delivery.Utils.TimeUtils;
import com.example.gaurav.delivery.entities.product_item;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import java.util.Calendar;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class Reports extends FragmentActivity {

    private static Context mContext;
   // Toolbar toolbar;
    AsyncHttpClient client;

    private reportadapter adapterViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

        Backendserver backend = new Backendserver(getApplicationContext());
        client = backend.getHTTPClient();

      //  toolbar = (Toolbar) findViewById(R.id.toolbarmi);
    //    ((AppCompatActivity)Reports()).getSupportActionBar().setTitle("titile");
     //   getS().setTitle("Material Issue");
     //   toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
      //  toolbar.requestFocus();

        mContext = this;

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);

        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        vpPager.setOffscreenPageLimit(1);

        vpPager.setAdapter(adapterViewPager);

        // set pager to current date
        vpPager.setCurrentItem(TimeUtils.getPositionForDay(Calendar.getInstance()));
    }

    public static class MyPagerAdapter extends reportadapter {

        private Calendar cal;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
          //  return TimeUtils.DAYS_OF_TIME;
            return 5;
        }

        @Override
        public Fragment getItem(int position) {

            long timeForPosition = TimeUtils.getDayForPosition(position).getTimeInMillis();
            return fragment_content.newInstance(timeForPosition);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Calendar cal = TimeUtils.getDayForPosition(position);
            return TimeUtils.getFormattedDate(mContext, cal.getTimeInMillis());
        }

    }
    public void getreport(){

        client.get(Backendserver.url + "/api/ecommerce/orderQtyMap/?courierAWBNo=", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toast.makeText(getApplicationContext(), "data not fetching" + errorResponse, Toast.LENGTH_SHORT);
            }
        });

    }

}