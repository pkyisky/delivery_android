package com.example.gaurav.delivery.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class Item {
    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getQuant() {
        return quant;
    }

    public void setQuant(String quant) {
        this.quant = quant;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String name1;
    public String quant;
    public String price;
    public String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String status1;
    public String grandTotal;

    JSONObject object;

    public Item(JSONObject object) throws JSONException {
        this.object = object;
        this.name1 = object.getString("name");
        this.price = object.getString("price");
        this.quant = object.getString("quantity");
        this.status1 = object.getString("status");
        this.grandTotal = object.getString("grandTotal");
        this.barcode = object.getString("barCVal");


    }


}
