package com.example.gaurav.delivery.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class product_item {

    public String pk;
    public String qty;
    public String productName;
    public String totalAmount;
    public String productPrice;
    public String ppAfterDiscount;
    public String discountAmount;

    public int amountotal;
    public String status;
    public String courierAWBNo;





    public String getPk() {


        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getPpAfterDiscount() {
        return ppAfterDiscount;
    }

    public void setPpAfterDiscount(String ppAfterDiscount) {
        this.ppAfterDiscount = ppAfterDiscount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JSONObject getObject() {
        return object;
    }

    public void setObject(JSONObject object) {
        this.object = object;
    }


    public int getAmountotal() {
        amountotal=Integer.parseInt(ppAfterDiscount)*Integer.parseInt(qty);


        return amountotal;
    }

    public void setAmountotal(int amountotal) {
        this.amountotal = amountotal;
    }


    public String getAmountpayable() {
        return amountpayable;
    }

    public void setAmountpayable(String amountpayable) {
        this.amountpayable = amountpayable;
    }

    public String amountpayable;

    public String getCourierAWBNo() {
        return courierAWBNo;
    }

    public void setCourierAWBNo(String courierAWBNo) {
        this.courierAWBNo = courierAWBNo;
    }


    JSONObject object;

    public product_item(JSONObject object) throws JSONException {
        this.object = object;
        this.pk = object.getString("pk");
        this.totalAmount = object.getString("totalAmount");
        this.productPrice = object.getString("productPrice");
        this.productName = object.getString("productName");
        this.ppAfterDiscount=object.getString("ppAfterDiscount");
        this.status=object.getString("status");
        this.qty=object.getString("qty");
        this.discountAmount=object.getString("discountAmount");
        this.amountpayable=object.getString("ppdiscount");
        this.courierAWBNo=object.getString("courierAWBNo");
    }




}
